<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Access the authenticated user via the Auth Facade
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    public function create()
    {
        //action to return a view containing a form for blog post creation
        return view('posts.create');
    }


    public function store(Request $request)
    {
        // if there is an authenticated user
        if(Auth::user()){
            // instantiate a new Post object from the Post Model
            $post = new Post;

            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');

            // get the id of the authenticated user and set it as the foreign key user_id of the new post
            $post->user_id = (Auth::user()->id);

            // save this post object in the database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }


    // action that will return a view showing all blog posts
    public function index()
    {
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }


    // action for showing only the posts authore by authenticated user
    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }
    
    // action that will return a view showing a specific post
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

}

















