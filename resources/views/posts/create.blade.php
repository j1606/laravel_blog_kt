@extends('layouts.app')

@section('content')
{{-- /posts/create --}}
{{-- To install the snippet for Laravel blade: ctrl shift P -> Package Control: Install Package -> Laravel Blade Highlight --}}

	<form method="POST" action="/posts">
		{{-- Cross-Site Request Forgery --}}
		@csrf

		<div class="form-group">
			<label for="title">Title:</label>
			<input type="text" class="form-control" id="title" name="title">
		</div>

		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" name="content" id="content" rows="3"></textarea>
		</div>

		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Create Post</button>
		</div>
	</form>
@endsection

{{-- Go to: /posts/create --}}



